﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;

namespace UDPSender
{
    public partial class Form1 : Form
    {
        short _sequence = 0;
        bool _pause = true;
        IPAddress _ip = IPAddress.None;
        byte[] _buffer = new byte[1];
        Socket _socket = null;
        IPEndPoint _endpoint = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _pause = !_pause;

            if (!_pause)
            {
                _sequence = 0;
                timer1.Interval = (int)numericUpDown2.Value;
                timer1.Enabled = true;
            }
            else
            {
                timer1.Enabled = false;
            }
            ValidateWindow();
        }

        void ValidateWindow()
        {
            if (_ip == IPAddress.None)
            {
                _pause = true;
                timer1.Enabled = false;
                button1.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
            }

            if (_pause)
            {
                timer1.Enabled = false;
                button1.Text = "Start";
            }
            else
            {
                button1.Text = "Pause";
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                _ip = IPAddress.Parse(textBox1.Text);
            }
            catch
            {
                _ip = IPAddress.None;
            }
            ValidateWindow();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            _socket.Ttl = 1;

            try
            {
                _ip = IPAddress.Parse(textBox1.Text);
            }
            catch
            {
                _ip = IPAddress.None;
            }
            ValidateWindow();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (_pause)
            {
                timer1.Enabled = false;
            }
            else
            {
                if (_buffer.Length != numericUpDown1.Value)
                {
                    _buffer = new byte[(int)numericUpDown1.Value];
                    for (int i = 0; i < numericUpDown1.Value; i++)
                    {
                        _buffer[i] = 0xAB;
                    }
                }

                if (_endpoint == null || _ip != _endpoint.Address || numericUpDown3.Value != _endpoint.Port)
                {
                    _endpoint = new IPEndPoint(_ip, (int)numericUpDown3.Value);
                    _sequence = 0;
                }

                if (timer1.Interval != (int)numericUpDown2.Value)
                {
                    timer1.Interval = (int)numericUpDown2.Value;
                }

                _buffer[0] = (byte)((_sequence & 0xFF00) >> 8);
                _buffer[1] = (byte)(_sequence & 0x00FF);
                label5.Text = _sequence.ToString();
                _sequence++;

                // send packet
                _socket.SendTo(_buffer, _endpoint);
            }
        }
    }
}
