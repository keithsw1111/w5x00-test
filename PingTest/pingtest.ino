// This minimal program allocates a MAC and and IP Address such that the arduino can respond to pings
// aim is to see if in that state it reliably responds to them

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif
#include <avr/pgmspace.h>

#include <SPI.h>
#include <Ethernet.h>

#define DEBUGSERIAL
#define USEW5100
//#define USEW5200

byte mac[6] = {0xDE, 0xBB, 0xCC, 0xDD, 0xEE, 188};
IPAddress ip(192,168,0,188); 

void setup()
{
	#if defined(DEBUGSERIAL) 
		Serial.begin(115200);
		Serial.println(F("Setup."));
		Serial.flush();
	#endif

	#ifdef USEW5100
		#if MAX_SOCK_NUM==8
			Compile error ... wrong w5100.h included
		#endif
	#endif
	
	#ifdef USEW5200
		#if MAX_SOCK_NUM==4
			Compile error ... wrong w5100.h included
		#endif
	#endif
	
	// disable the W5100 card
	pinMode(10, OUTPUT);                       
	digitalWrite(10, HIGH);                    

	#ifdef DEBUGSERIAL
		Serial.println(F("Initialising ethernet ..."));
		Serial.print(F("Requested IP Address "));
		Serial.println(ip);
		Serial.flush();
	#endif

	delay(2000);

	Ethernet.begin(mac, ip);
	
	// delay to let IP address take effect? Sounds bogus but I am desperate
	delay(100);

	#ifdef DEBUGSERIAL
		Serial.print(F("MAX_SOCK_NUM = "));
		Serial.println(MAX_SOCK_NUM);
		
		Serial.print(F("Allocated IP Address "));
		Serial.println(Ethernet.localIP());
	#endif
	
	if (Ethernet.localIP() == ip)
	{
		Serial.println(F("Ready."));
	}
	else
	{
		Serial.println(F("Ethernet did not initialise correctly."));
	}
}

void loop()
{
	#if defined(DEBUGSERIAL) 
		Serial.println(F("Loop."));
		Serial.flush();
	#endif

	// do absolutely nothing
	delay(2000);
}