// This minimal program receives a UDP packet and displays the sequence number in the first 2 bytes
// aim is to see if in that state it reliably receives them

#if defined(ARDUINO) && ARDUINO >= 100
	#include "Arduino.h"
#else
	#include "WProgram.h"
#endif
#include <avr/pgmspace.h>

#include <SPI.h>
// THIS MAY NEED TO CHANGE TO A MORE APPROPRIATE ETHERNET LIBRARY
#include <Ethernet.h>
#include <utility/socket.h>

#define DEBUGSERIAL
//#define USEW5100
#define USEW5200

// THIS CAN ONLY BE USED WITH MY CUSTOMISED ETHERNET LIBRARY WITH EXTENDED DEBUG FUNCTIONALITY
//#define ADVANCEDCONFIG

#ifdef USEW5200
	#define RESETW5200
#endif

#ifdef RESETW5200
	#define RST 8
	#define PWDN 9
#endif

byte mac[6] = {0xDE, 0xBB, 0xCC, 0xDD, 0xEE, 188};
IPAddress ip(192,168,0,188); 
int port = 5568;
EthernetUDP _Udp;
int lastsequence = -1;

void setup()
{
 	// disable the SD card by switching pin 4 high
    // not using the SD card in this program, but if an SD card is left in the socket,
    // it may cause a problem with accessing the Ethernet chip, unless disabled
    pinMode(4, OUTPUT);
    digitalWrite(4, HIGH);
	
	// disable the W5100 card
	pinMode(10, OUTPUT);                       
	digitalWrite(10, HIGH);                    

	// Stop SS from floating
	pinMode(SS, OUTPUT);

	#if defined(DEBUGSERIAL) 
		Serial.begin(115200);
		Serial.println(F("Setup."));
		Serial.flush();
	#endif

	#ifdef USEW5100
		#if MAX_SOCK_NUM==8
			Compile error ... wrong w5100.h included
		#endif
	#endif
	
	#ifdef USEW5200
		#if MAX_SOCK_NUM==4
			// YOU MAY NEED TO COMMENT THIS OUT IF YOUR LIBRARY DOES NOT SET THIS CORRECTLY
			Compile error ... wrong w5100.h included
		#endif
	#endif
	
	bool ethernetok = false;

	while(!ethernetok)
	{
		#ifdef RESETW5200
			pinMode(RST,OUTPUT);
			pinMode(PWDN,OUTPUT);
			// This code may reset the W5200 chip ... and I hope wont hurt the W5100 chip
			digitalWrite(PWDN,LOW); //enable power
			digitalWrite(RST,LOW);  //Reset W5200
			delay(10);
			digitalWrite(RST,HIGH);  
			delay(200);      		// wait W5200 work
		#endif

		#ifdef DEBUGSERIAL
			Serial.println(F("Initialising ethernet ..."));
			Serial.print(F("MAC Address {"));
			for(int i =0; i < sizeof(mac)-1; i++)
			{
				Serial.print(mac[i], HEX);
				Serial.print(F(","));
			}
			Serial.print(mac[sizeof(mac)-1], HEX);
			Serial.println(F("}"));
			Serial.print(F("Requested IP Address "));
			Serial.println(ip);
			Serial.flush();
		#endif

		delay(2000);
	
		Ethernet.begin(mac, ip);
	
		// delay to let IP address take effect? Sounds bogus but I am desperate
		delay(100);

		#ifdef DEBUGSERIAL
			Serial.print(F("MAX_SOCK_NUM = "));
			Serial.println(MAX_SOCK_NUM);
			
			Serial.print(F("Allocated IP Address "));
			Serial.println(Ethernet.localIP());
		#endif
	
		if (Ethernet.localIP() == ip)
		{
			ethernetok = true;
		}
		else
		{
			Serial.println(F("Ethernet did not initialise correctly ... trying again."));
	
			#ifdef ADVANCEDCONFIG
				#ifdef DEBUGSERIAL
					Ethernet.DumpState();
				#endif
			#endif
			
			delay(500);
		}
	}
	
	#ifdef DEBUGSERIAL
		Serial.print(F("UDP Listening on port "));
		Serial.print(port);
	#endif
	
	if (_Udp.begin(port) == 0)
	{
		#ifdef DEBUGSERIAL
			Serial.println(F("Error opening UDP listener."));
			Serial.println(F("Stopping."));
		#endif
		while(true);
	}
	
	#ifdef ADVANCEDCONFIG
		// set large receive buffer
		if (MAX_SOCK_NUM == 8)
		{
			SetRXBufferSize(0, 16);
		}
		else
		{
			SetRXBufferSize(0, 8);
		}
		for (int i = 1; i < MAX_SOCK_NUM; i++) 
		{
			SetRXBufferSize(i, 0);
		}

		#ifdef DEBUGSERIAL
			Ethernet.DumpState();
		#endif
	#endif
}

void loop()
{
	int packetSize = _Udp.parsePacket();

	if (packetSize >= 2)
	{
		#ifdef DEBUGSERIAL
			Serial.print(F("Packet received. Size: "));
		#endif
		
		short s;
		byte b1;
		byte b2;
		_Udp.read((unsigned char*)&b1, sizeof(b1));
		_Udp.read((unsigned char*)&b2, sizeof(b2));
		s = (((short)b1) << 8) + b2;
		
		#ifdef DEBUGSERIAL
			Serial.print(packetSize);
			Serial.print(F(" Sequence: "));
			Serial.print(s);
			if (s == lastsequence + 1)
			{
				Serial.println("");
			}
			else
			{
				Serial.print(F(" missed packets: "));
				Serial.println(s - lastsequence - 1);
			}
			Serial.flush();
		#endif
		lastsequence = s;
	}
	else if (packetSize < 0)
	{
		#ifdef DEBUGSERIAL
			Serial.print(F("Negative sized packet ... not a good sign: "));
			Serial.println(packetSize);
			Serial.flush();
		#endif
	}
	else if (packetSize == 1)
	{
		#ifdef DEBUGSERIAL
			Serial.print(F("Odd sized packet: "));
			Serial.println(packetSize);
			Serial.flush();
		#endif
	}
}