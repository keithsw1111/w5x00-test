This project consists of the following folders:

	Arduino sketch ... this is the arduino code which listens for UDP packets and writes out to the serial port when it sees them and shows how many go missing. This works with the C# UDP Packet Generator code.
	
	C# UDP Packet Generator ... this windows program generates UDP packets which should be sent to the arduino. You can control packet size and delay between packets. This works with the Arduino Sketch folder code. I have included both the source and binary versions.
	
	Pingtest ... a simple sketch that just gives the arduino an IP addess. I then ping the arduino to see how effectively it responds. Best I have managed is about 48% ... it is often much much worse.
	
	Extended ethernet library ... this is an extended version of the wiznet ethernet library i have been working on which adds the following features:
		- Correctly identifies the number of available sockets.
		- Allows you to manage the allocation of W5x00 buffers to sockets. Technically you could allocate all 16K to the UDP receive buffer. By default each receive buffer is 2K and with all other libraries cannot be changed.
		- Provides debug functions which query the wiznet chip directly to understand what is going on on the chip. These functions can for instance show if the receive buffer has filled up.
		- Optimises SPI for the arduino mega & W5200 to ensure the best possible read speed between the 2 boards. This may not work on the Uno.
		- To the best of my knowledge these are based on the best and most current wiznet libraries. Problems others have reported seem to have been fixed in it.
	
	When I run this on my mega i see blocks of success (maybe 10 packets in a row), periods when 1-2 packets are being lost regularly and the occasionally long pauses when lots of packets are lost.
	
	Problems occur on both W5100 and W5200 boards. I have never tested W5500 boards.
	
	I am looking for someone to test it on their config and hopefully tell me it is perfect or very near perfect because that would confirm it is my problem somewhere.
	